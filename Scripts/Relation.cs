﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Relation {
    public enum RelationType
    {
        BESTMATE,
        FRIEND,
        ACQUAINTANCE,
        NEUTRAL,
        FRENEMY,
        ENEMY,
        ARCHNEMESIS,
        NONE
    }

    #region - Variables.
    // tier 1 = 10 points, tier 2 = 100, tier 3 = 500
    // Based on relation to other person, there could be a level 
    // of resistance to your gain in standing with them.
    public int relationPoints;

    public RelationType relationType;
    #endregion


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public Relation()
    {
        relationPoints = 0;

        relationType = RelationType.NEUTRAL;
    }


    public void ChangeRelationAmount(int relationAmount)
    {
        relationPoints += relationAmount;
    }
}
