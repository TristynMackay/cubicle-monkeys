﻿using System.Collections.Generic;

public class PersonalityProfile_Player : PersonalityProfile {


    /// <summary>
    /// Default Constructor.
    /// 
    /// Use this when creating a player worker.
    /// </summary>
    public PersonalityProfile_Player()
    {
        personalityProfileType = PersonalityProfileType.NONE;

        personalityTraits = new List<PersonalityTrait>();
    }
}
