﻿using System.Collections.Generic;

class PersonalityProfile_PassiveAggressivePrick : PersonalityProfile {
    

    /// <summary>
    /// Default Constructor.
    /// 
    /// Use this when creating an A.I. worker.
    /// </summary>
    public PersonalityProfile_PassiveAggressivePrick()
    {
        personalityProfileType = PersonalityProfileType.PASSIVEAGGRESIVEPRICK;

        personalityTraits = new List<PersonalityTrait>();

        personalityTraits.Add(new PersonalityTrait_Boaster(5));
        personalityTraits.Add(new PersonalityTrait_LooseLipped(2));
        personalityTraits.Add(new PersonalityTrait_Pessimistic(3));
        personalityTraits.Add(new PersonalityTrait_StuckUp(4));
        personalityTraits.Add(new PersonalityTrait_Ignorant(4));
        personalityTraits.Add(new PersonalityTrait_Honest(3));
        personalityTraits.Add(new PersonalityTrait_Joker(2));
        personalityTraits.Add(new PersonalityTrait_Defensive(4));
    }
}
