﻿using System.Collections.Generic;
using UnityEngine;

public class DecisionHistory : MonoBehaviour {

    List<Decision> decisions;


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public DecisionHistory()
    {
        decisions = new List<Decision>();
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    public DecisionHistory(List<Decision> decisions)
    {
        this.decisions = decisions;
    }
}
