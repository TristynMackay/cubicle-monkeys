﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PersonalityProfile : MonoBehaviour {

    public enum PersonalityProfileType
    {
        PASSIVEAGGRESIVEPRICK,
        PSYCOPATH,
        DOWNRIGHTDICK,
        ROBOT,
        WORKAHOLIC,
        BACKSTABBER,
        TYRANT,
        SLAVEMASTER,
        GOSSIPER,
        SURVIVALIST,
        WORKPLACECLOWN,
        SLAVE,
        NONE
    }


    #region - Variables.
    public PersonalityProfileType personalityProfileType;

    public List<PersonalityTrait> personalityTraits;
    #endregion
}
