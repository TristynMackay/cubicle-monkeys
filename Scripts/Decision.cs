﻿using System.Collections.Generic;

public class Decision {

	public enum DecisionType
    {
        BEADICK,
        CONFORMTOTHENORM,
        BROWNNOSE,
        NONE
    }

    #region - Variables.
    public List<DecisionAction> decisions;
    #endregion
}
