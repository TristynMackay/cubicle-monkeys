﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionAction {

    #region - Variables.
    // Influence points determine how much someone may like or dislike this action (alignment + - or neutral).
    public int influencePoints;

    public Decision.DecisionType decisionType;

    public string decisionTypeName;
    public string decisionTypeDescription;

    public BusinessRole businessRoleRequirement;    // If the role of the worker is equal to or greater than this role, then perform this action.

    public string actionName;
    public string actionDescription;

    public List<PersonalityTrait.PersonalityTraitType> positiveInfluenceTraits;
    public List<PersonalityTrait.PersonalityTraitType> negativeInfluenceTraits;

    // If relationPointRequirement is 0, show it by default.
    public int relationPointRequirement;
    #endregion


    // Set the decisionTypeDescription.
    public void setDecisionTypeDescription()
    {
        switch (decisionType)
        {
            case Decision.DecisionType.BEADICK:
                decisionTypeName = "Be A Dick!";
                decisionTypeDescription = "Make a dick move that everyone will most likely hate you for.";
                break;
            case Decision.DecisionType.CONFORMTOTHENORM:
                decisionTypeName = "Conform To The Norm!";
                decisionTypeDescription = "Conform to the rules put in place by the man.";
                break;
            case Decision.DecisionType.BROWNNOSE:
                decisionTypeName = "Brown Nose!";
                decisionTypeDescription = "Please your subordinates, peers, and superiors by sucking up as hard as you can.";
                break;
            default:
                break;
        }
    }
}
