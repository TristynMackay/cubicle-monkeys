﻿using System.Collections.Generic;

public class DecisionAction_ShitOnDesk : DecisionAction {

	
    /// <summary>
    /// Default Constructor.
    /// </summary>
    public DecisionAction_ShitOnDesk(Business.BusinessType businessType)
    {
        actionName = "Shit On Desk!";
        
        influencePoints = 40;

        relationPointRequirement = 0;

        switch(businessType)
        {
            case Business.BusinessType.ITCONSULTANCY:
                businessRoleRequirement = new BusinessRole(Business.BusinessType.ITCONSULTANCY,
                    BusinessRole.ITConsultancy.TEAMMEMBER.ToString());
                break;
            default:
                break;
        }

        decisionType = Decision.DecisionType.BEADICK;
        setDecisionTypeDescription();

        actionDescription = "Take a shit on the someone’s desk to let them know what you think of them, " +
            "they might not find out who did it, or will they?";

        // Create positive influence traits for this action.
        positiveInfluenceTraits = new List<PersonalityTrait.PersonalityTraitType>();

        positiveInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.JOKER);
        positiveInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.OPTIMISTIC);

        // Create negative influence traits for this action.
        negativeInfluenceTraits = new List<PersonalityTrait.PersonalityTraitType>();

        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.SHUTIN);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.COMMITTED);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.LEADER);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.BOSSY);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.TEMPERAMENTAL);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.CONTROLFREAK);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.SKITTISH);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.PESSIMISTIC);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.REALISTIC);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.DEFENSIVE);
    }
}
