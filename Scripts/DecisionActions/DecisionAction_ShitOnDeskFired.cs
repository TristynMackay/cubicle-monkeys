﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionAction_ShitOnDeskFired : DecisionAction {

	
    /// <summary>
    /// Default Constructor.
    /// </summary>
    public DecisionAction_ShitOnDeskFired(Business.BusinessType businessType)
    {
        actionName = "Shit On Desk!";

        influencePoints = 120;

        relationPointRequirement = 0;
        
        switch (businessType)
        {
            case Business.BusinessType.ITCONSULTANCY:
                businessRoleRequirement = new BusinessRole(Business.BusinessType.ITCONSULTANCY,
                    BusinessRole.ITConsultancy.TEAMMEMBER.ToString());
                break;
            default:
                break;
        }

        decisionType = Decision.DecisionType.BEADICK;
        setDecisionTypeDescription();

        actionDescription = "Take a shit on the boss’s desk in front of them, they pissed you off unbelievably," +
            "this is just a taste of all the hatred you have toward them, you couldn’t give a shit if you can’t use " +
            "them as a character reference, you’ve already got a million and one other references anyway.";

        // Create positive influence traits for this action.
        positiveInfluenceTraits = new List<PersonalityTrait.PersonalityTraitType>();

        positiveInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.JOKER);
        positiveInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.OPTIMISTIC);

        // Create negative influence traits for this action.
        negativeInfluenceTraits = new List<PersonalityTrait.PersonalityTraitType>();

        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.SHUTIN);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.COMMITTED);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.LEADER);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.BOSSY);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.TEMPERAMENTAL);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.CONTROLFREAK);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.SKITTISH);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.PESSIMISTIC);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.REALISTIC);
        negativeInfluenceTraits.Add(PersonalityTrait.PersonalityTraitType.DEFENSIVE);
    }
}
