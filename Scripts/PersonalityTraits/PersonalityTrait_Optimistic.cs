﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Optimistic : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Optimistic()
    {
        personalityTraitName = "Optimistic";
        personalityTraitType = PersonalityTraitType.OPTIMISTIC;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Optimistic(int currentTraitValue)
    {
        personalityTraitName = "Optimistic";
        personalityTraitType = PersonalityTraitType.OPTIMISTIC;
        this.currentTraitValue = currentTraitValue;
    }
}
