﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_StuckUp : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_StuckUp()
    {
        personalityTraitName = "Stuck Up";
        personalityTraitType = PersonalityTraitType.STUCKUP;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_StuckUp(int currentTraitValue)
    {
        personalityTraitName = "Stuck Up";
        personalityTraitType = PersonalityTraitType.STUCKUP;
        this.currentTraitValue = currentTraitValue;
    }
}
