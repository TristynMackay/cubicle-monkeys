﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_SexualDeviant : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_SexualDeviant()
    {
        personalityTraitName = "Sexual Deviant";
        personalityTraitType = PersonalityTraitType.SELFISH;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_SexualDeviant(int currentTraitValue)
    {
        personalityTraitName = "Sexual Deviant";
        personalityTraitType = PersonalityTraitType.SELFISH;
        this.currentTraitValue = currentTraitValue;
    }
}
