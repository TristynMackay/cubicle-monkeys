﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Lazy : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Lazy()
    {
        personalityTraitName = "Lazy";
        personalityTraitType = PersonalityTraitType.LAZY;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Lazy(int currentTraitValue)
    {
        personalityTraitName = "Lazy";
        personalityTraitType = PersonalityTraitType.LAZY;
        this.currentTraitValue = currentTraitValue;
    }
}
