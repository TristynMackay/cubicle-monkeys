﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_ControlFreak : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_ControlFreak()
    {
        personalityTraitName = "Control Freak";
        personalityTraitType = PersonalityTraitType.CONTROLFREAK;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_ControlFreak(int currentTraitValue)
    {
        personalityTraitName = "Control Freak";
        personalityTraitType = PersonalityTraitType.CONTROLFREAK;
        this.currentTraitValue = currentTraitValue;
    }
}
