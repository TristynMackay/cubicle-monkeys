﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Brave : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Brave()
    {
        personalityTraitName = "Brave";
        personalityTraitType = PersonalityTraitType.BRAVE;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Brave(int currentTraitValue)
    {
        personalityTraitName = "Brave";
        personalityTraitType = PersonalityTraitType.BRAVE;
        this.currentTraitValue = currentTraitValue;
    }
}
