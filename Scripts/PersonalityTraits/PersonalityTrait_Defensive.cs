﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Defensive : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Defensive()
    {
        personalityTraitName = "Defensive";
        personalityTraitType = PersonalityTraitType.DEFENSIVE;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Defensive(int currentTraitValue)
    {
        personalityTraitName = "Defensive";
        personalityTraitType = PersonalityTraitType.DEFENSIVE;
        this.currentTraitValue = currentTraitValue;
    }
}
