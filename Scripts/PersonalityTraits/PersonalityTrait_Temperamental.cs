﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Temperamental : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Temperamental()
    {
        personalityTraitName = "Temperamental";
        personalityTraitType = PersonalityTraitType.TEMPERAMENTAL;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Temperamental(int currentTraitValue)
    {
        personalityTraitName = "Temperamental";
        personalityTraitType = PersonalityTraitType.TEMPERAMENTAL;
        this.currentTraitValue = currentTraitValue;
    }
}
