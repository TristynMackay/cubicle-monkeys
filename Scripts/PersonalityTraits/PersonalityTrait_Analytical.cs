﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Analytical : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Analytical()
    {
        personalityTraitName = "Analytical";
        personalityTraitType = PersonalityTraitType.ANALYTICAL;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Analytical(int currentTraitValue)
    {
        personalityTraitName = "Analytical";
        personalityTraitType = PersonalityTraitType.ANALYTICAL;
        this.currentTraitValue = currentTraitValue;
    }
}
