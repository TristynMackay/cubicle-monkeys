﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Bossy : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Bossy()
    {
        personalityTraitName = "Bossy";
        personalityTraitType = PersonalityTraitType.BOSSY;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Bossy(int currentTraitValue)
    {
        personalityTraitName = "Bossy";
        personalityTraitType = PersonalityTraitType.BOSSY;
        this.currentTraitValue = currentTraitValue;
    }
}
