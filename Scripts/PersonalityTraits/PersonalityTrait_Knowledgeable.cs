﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Knowledgeable : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Knowledgeable()
    {
        personalityTraitName = "Knowledgeable";
        personalityTraitType = PersonalityTraitType.KNOWLEDGEABLE;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Knowledgeable(int currentTraitValue)
    {
        personalityTraitName = "Knowledgeable";
        personalityTraitType = PersonalityTraitType.KNOWLEDGEABLE;
        this.currentTraitValue = currentTraitValue;
    }
}
