﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Empathetic : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Empathetic()
    {
        personalityTraitName = "Empathetic";
        personalityTraitType = PersonalityTraitType.EMPATHETIC;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Empathetic(int currentTraitValue)
    {
        personalityTraitName = "Empathetic";
        personalityTraitType = PersonalityTraitType.EMPATHETIC;
        this.currentTraitValue = currentTraitValue;
    }
}
