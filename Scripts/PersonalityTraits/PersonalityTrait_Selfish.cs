﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Selfish : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Selfish()
    {
        personalityTraitName = "Selfish";
        personalityTraitType = PersonalityTraitType.SELFISH;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Selfish(int currentTraitValue)
    {
        personalityTraitName = "Selfish";
        personalityTraitType = PersonalityTraitType.SELFISH;
        this.currentTraitValue = currentTraitValue;
    }
}
