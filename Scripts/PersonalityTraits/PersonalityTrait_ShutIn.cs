﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_ShutIn : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_ShutIn()
    {
        personalityTraitName = "Shut In";
        personalityTraitType = PersonalityTraitType.SHUTIN;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_ShutIn(int currentTraitValue)
    {
        personalityTraitName = "Shut In";
        personalityTraitType = PersonalityTraitType.SHUTIN;
        this.currentTraitValue = currentTraitValue;
    }
}
