﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Pessimistic : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Pessimistic()
    {
        personalityTraitName = "Pessimistic";
        personalityTraitType = PersonalityTraitType.PESSIMISTIC;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Pessimistic(int currentTraitValue)
    {
        personalityTraitName = "Pessimistic";
        personalityTraitType = PersonalityTraitType.PESSIMISTIC;
        this.currentTraitValue = currentTraitValue;
    }
}
