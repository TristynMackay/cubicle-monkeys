﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Honest : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Honest()
    {
        personalityTraitName = "Honest";
        personalityTraitType = PersonalityTraitType.HONEST;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Honest(int currentTraitValue)
    {
        personalityTraitName = "Honest";
        personalityTraitType = PersonalityTraitType.HONEST;
        this.currentTraitValue = currentTraitValue;
    }
}
