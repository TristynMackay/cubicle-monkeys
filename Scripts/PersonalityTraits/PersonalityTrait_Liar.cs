﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Liar : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Liar()
    {
        personalityTraitName = "Liar";
        personalityTraitType = PersonalityTraitType.LIAR;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Liar(int currentTraitValue)
    {
        personalityTraitName = "Liar";
        personalityTraitType = PersonalityTraitType.LIAR;
        this.currentTraitValue = currentTraitValue;
    }
}
