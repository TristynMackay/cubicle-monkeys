﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Joker : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Joker()
    {
        personalityTraitName = "Joker";
        personalityTraitType = PersonalityTraitType.JOKER;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Joker(int currentTraitValue)
    {
        personalityTraitName = "Joker";
        personalityTraitType = PersonalityTraitType.JOKER;
        this.currentTraitValue = currentTraitValue;
    }
}
