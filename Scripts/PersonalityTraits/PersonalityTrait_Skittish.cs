﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Skittish : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Skittish()
    {
        personalityTraitName = "Skittish";
        personalityTraitType = PersonalityTraitType.SKITTISH;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Skittish(int currentTraitValue)
    {
        personalityTraitName = "Skittish";
        personalityTraitType = PersonalityTraitType.SKITTISH;
        this.currentTraitValue = currentTraitValue;
    }
}
