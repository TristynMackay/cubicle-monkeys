﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Loyal : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Loyal()
    {
        personalityTraitName = "Loyal";
        personalityTraitType = PersonalityTraitType.LOYAL;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Loyal(int currentTraitValue)
    {
        personalityTraitName = "Loyal";
        personalityTraitType = PersonalityTraitType.LOYAL;
        this.currentTraitValue = currentTraitValue;
    }
}
