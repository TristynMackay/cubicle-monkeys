﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_LooseLipped : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_LooseLipped()
    {
        personalityTraitName = "Loose Lipped";
        personalityTraitType = PersonalityTraitType.LOOSELIPPED;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_LooseLipped(int currentTraitValue)
    {
        personalityTraitName = "Loose Lipped";
        personalityTraitType = PersonalityTraitType.LOOSELIPPED;
        this.currentTraitValue = currentTraitValue;
    }
}
