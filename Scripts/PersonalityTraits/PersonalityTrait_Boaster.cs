﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Boaster : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Boaster()
    {
        personalityTraitName = "Boaster";
        personalityTraitType = PersonalityTraitType.BOASTER;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Boaster(int currentTraitValue)
    {
        personalityTraitName = "Boaster";
        personalityTraitType = PersonalityTraitType.BOASTER;
        this.currentTraitValue = currentTraitValue;
    }
}
