﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Nosey : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Nosey()
    {
        personalityTraitName = "Nosey";
        personalityTraitType = PersonalityTraitType.NOSEY;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Nosey(int currentTraitValue)
    {
        personalityTraitName = "Nosey";
        personalityTraitType = PersonalityTraitType.NOSEY;
        this.currentTraitValue = currentTraitValue;
    }
}
