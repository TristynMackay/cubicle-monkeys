﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Committed : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Committed()
    {
        personalityTraitName = "Committed";
        personalityTraitType = PersonalityTraitType.COMMITTED;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Committed(int currentTraitValue)
    {
        personalityTraitName = "Committed";
        personalityTraitType = PersonalityTraitType.COMMITTED;
        this.currentTraitValue = currentTraitValue;
    }
}
