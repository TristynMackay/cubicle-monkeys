﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Realistic : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Realistic()
    {
        personalityTraitName = "Realistic";
        personalityTraitType = PersonalityTraitType.REALISTIC;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Realistic(int currentTraitValue)
    {
        personalityTraitName = "Realistic";
        personalityTraitType = PersonalityTraitType.REALISTIC;
        this.currentTraitValue = currentTraitValue;
    }
}
