﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Innovative : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Innovative()
    {
        personalityTraitName = "Innovative";
        personalityTraitType = PersonalityTraitType.INNOVATIVE;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Innovative(int currentTraitValue)
    {
        personalityTraitName = "Innovative";
        personalityTraitType = PersonalityTraitType.INNOVATIVE;
        this.currentTraitValue = currentTraitValue;
    }
}
