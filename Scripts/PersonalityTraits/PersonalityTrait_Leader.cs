﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Leader : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Leader()
    {
        personalityTraitName = "Leader";
        personalityTraitType = PersonalityTraitType.LEADER;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Leader(int currentTraitValue)
    {
        personalityTraitName = "Leader";
        personalityTraitType = PersonalityTraitType.LEADER;
        this.currentTraitValue = currentTraitValue;
    }
}
