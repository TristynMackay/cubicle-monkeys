﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Blamer : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Blamer()
    {
        personalityTraitName = "Blamer";
        personalityTraitType = PersonalityTraitType.BLAMER;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Blamer(int currentTraitValue)
    {
        personalityTraitName = "Blamer";
        personalityTraitType = PersonalityTraitType.BLAMER;
        this.currentTraitValue = currentTraitValue;
    }
}
