﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonalityTrait_Ignorant : PersonalityTrait {


    /// <summary>
    /// Default Constructor.
    /// </summary>
    public PersonalityTrait_Ignorant()
    {
        personalityTraitName = "Ignorant";
        personalityTraitType = PersonalityTraitType.IGNORANT;
        currentTraitValue = 0;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    /// <param name="currentTraitValue"></param>
    public PersonalityTrait_Ignorant(int currentTraitValue)
    {
        personalityTraitName = "Ignorant";
        personalityTraitType = PersonalityTraitType.IGNORANT;
        this.currentTraitValue = currentTraitValue;
    }
}
