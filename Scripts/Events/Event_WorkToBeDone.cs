﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_WorkToBeDone : Event {


    public Event_WorkToBeDone(Business.BusinessType businessType)
    {
        eventName = "There's Work To Be Done!";

        description = "Your boss has come to you with a pile of work to be done and dumps it on your desk, what do you do?";

        decision = new Decision_WorkToBeDone(businessType);
    }
}
