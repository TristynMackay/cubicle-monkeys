﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Event {

    #region - Variables.
    public string eventName;
    public string description;

    public Worker EventGiver;
    public List<Worker> EventReceivers;

    public Decision decision;
    #endregion


    public Event()
    {

    }
}
