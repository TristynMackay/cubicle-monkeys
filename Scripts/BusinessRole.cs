﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusinessRole {

    #region Variables.
    public string businessRole;
    public int businessRoleNumber;
    #endregion


    /*  
     *   GAMEDEVELOPMENT,       - S, M, L
     *   HEALTHCLINIC,          - S, M, L
     *   ITCONSULTANCY,         - S, M, L   MADE
     *   INSURANCE,             - M, L
     *   MEDIA,                 - S, M, L
     *   LABORATORY,            - S, M, L
     *   PRIMARYSCHOOL,         - S, M, L
     *   HIGHSCHOOL,            - S, M, L
     *   UNIVERSITY,            - L
     *   COMMUNITYCOLLEGE,      - L
     *   FOODCHAIN,             - S, M, L   MADE
     *   RETAILER,              - S, M, L   MADE
     *   AGEDCARE,              - S, M, L
     *   HOSPITAL,              - M, L
     *   HOSPITALITY,           - S, M, L
     *   GOVERNMENT,            - L
     *   COUNCIL,               - M, L
     *   WELFARE,               - M, L
     */


    public enum ITConsultancy
    {
        CEO,
        PROJECTMANAGER,
        PROJECTASSISTANT,
        TEAMMANAGER,
        TEAMMEMBER,
        UNEMPLOYED
    }

    public enum FoodChainBusinessRole
    {
        REGIONALMANAGER,
        STOREMANAGER,
        TEAMMANAGER,
        TEAMMEMBER,
        UNEMPLOYED
    }

    public enum RetailerBusinessRole
    {
        REGIONALMANAGER,
        STOREMANAGER,
        TEAMMANAGER,
        TEAMMEMBER,
        UNEMPLOYED
    }

    public enum GameDevelopmentBusinessRole
    {
        CEO,
        PROJECTMANAGER,
        PROJECTASSISTANT,
        TEAMMANAGER,
        TEAMMEMBER,
        UNEMPLOYED
    }

    public enum HospitalityBusinessRole
    {
        CEO,
        PROJECTMANAGER,
        PROJECTASSISTANT,
        TEAMMANAGER,
        TEAMMEMBER,
        UNEMPLOYED
    }

    public enum ITConsultancyBusinessRole
    {
        CEO,
        PROJECTMANAGER,
        PROJECTASSISTANT,
        TEAMMANAGER,
        TEAMMEMBER,
        UNEMPLOYED
    }


    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    public BusinessRole (Business.BusinessType businessType, string businessRole)
    {
        this.businessRole = businessRole;

        switch(businessType)
        {
            case Business.BusinessType.ITCONSULTANCY:
                switch (businessRole)
                {
                    case "CEO":
                        businessRoleNumber = (int)BusinessRole.ITConsultancyBusinessRole.CEO;
                        break;
                    case "PROJECTMANAGER":
                        businessRoleNumber = (int)BusinessRole.ITConsultancyBusinessRole.PROJECTMANAGER;
                        break;
                    case "PROJECTASSISTANT":
                        businessRoleNumber = (int)BusinessRole.ITConsultancyBusinessRole.PROJECTASSISTANT;
                        break;
                    case "TEAMMANAGER":
                        businessRoleNumber = (int)BusinessRole.ITConsultancyBusinessRole.TEAMMANAGER;
                        break;
                    case "TEAMMEMBER":
                        businessRoleNumber = (int)BusinessRole.ITConsultancyBusinessRole.TEAMMEMBER;
                        break;
                    case "UNEMPLOYED":
                        businessRoleNumber = (int)BusinessRole.ITConsultancyBusinessRole.UNEMPLOYED;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
}
