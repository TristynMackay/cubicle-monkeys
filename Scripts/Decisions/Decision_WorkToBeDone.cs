﻿using System.Collections.Generic;

public class Decision_WorkToBeDone : Decision {

	
    /// <summary>
    /// Default Constructor.
    /// </summary>
    public Decision_WorkToBeDone(Business.BusinessType businessType)
    {
        decisions = new List<DecisionAction>();

        decisions.Add(new DecisionAction_ShitOnDesk(businessType));
    }
}
