﻿using System.Collections.Generic;

public class Decision_QuittingTime : Decision {

	
    /// <summary>
    /// Default Constructor.
    /// </summary>
    public Decision_QuittingTime(Business.BusinessType businessType)
    {
        decisions = new List<DecisionAction>();

        decisions.Add(new DecisionAction_ShitOnDeskFired(businessType));
    }
}
