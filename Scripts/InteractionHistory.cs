﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionHistory : MonoBehaviour {
    #region - Variables.
    List<Interaction> interactions;
    #endregion


    #region - Constructors.
    /// <summary>
    /// Default Constructor.
    /// </summary>
    public InteractionHistory()
    {
        interactions = new List<Interaction>();
    }

    /// <summary>
    /// Overloaded Constructor.
    /// </summary>
    public InteractionHistory(List<Interaction> interactions)
    {
        this.interactions = interactions;
    }
    #endregion
}
