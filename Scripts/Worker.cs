﻿

public class Worker {


    #region - Variables.
    public PersonalityProfile personality;

    public BusinessRole businessRole;
    #endregion


    /// <summary>
    /// Default Constructor
    /// 
    /// Use this to create a player worker.
    /// </summary>
    public Worker(BusinessRole businessRole)
    {
        personality = new PersonalityProfile_Player();
        this.businessRole = businessRole;
    }

    /// <summary>
    /// Overloaded Constructor.
    /// 
    /// Use this to create an A.I.
    /// </summary>
    public Worker(PersonalityProfile personality, BusinessRole businessRole)
    {
        this.personality = personality;
        this.businessRole = businessRole;
    }
}
