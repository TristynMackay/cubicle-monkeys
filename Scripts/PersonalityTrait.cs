﻿

public abstract class PersonalityTrait {

    public enum PersonalityTraitType
    {
        SELFISH,
        LOOSELIPPED,
        SHUTIN,
        BOASTER,
        COMMITTED,
        LEADER,
        BOSSY,
        LIAR,
        HONEST,
        BLAMER,
        LAZY,
        TEMPERAMENTAL,
        CONTROLFREAK,
        SKITTISH,
        BRAVE,
        PESSIMISTIC,
        REALISTIC,
        OPTIMISTIC,
        INNOVATIVE,
        ANALYTICAL,
        IGNORANT,
        KNOWLEDGEABLE,
        NOSEY,
        DEFENSIVE,
        STUCKUP,
        JOKER,
        EMPATHETIC,
        LOYAL,
        SEXUALDEVIANT,
        NONE
    }


    #region - Variables.
    public string personalityTraitName;

    public PersonalityTraitType personalityTraitType;

    public int currentTraitValue = 0;
    #endregion
}
