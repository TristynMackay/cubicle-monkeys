﻿using System.Collections;
using System.Collections.Generic;

public class Business {

    #region - Variables.
    public int numberOfWorkers;
    public List<Worker> workers;

    public BusinessSize businessSize;
    public BusinessType businessType;

    public List<BusinessRole> businessRoles;
    #endregion

    public enum BusinessSize
    {
        SMALL,
        MEDIUM,
        LARGE,
        NONE
    }

    public enum BusinessType
    {
        GAMEDEVELOPMENT,
        HEALTHCLINIC,
        ITCONSULTANCY,
        INSURANCE,
        MEDIA,
        LABORATORY,
        PRIMARYSCHOOL,
        HIGHSCHOOL,
        UNIVERSITY,
        COMMUNITYCOLLEGE,
        FOODCHAIN,
        RETAILER,
        AGEDCARE,
        HOSPITAL,
        HOSPITALITY,
        GOVERNMENT,
        COUNCIL,
        WELFARE,
        NONE
    }

    // Small business is 20 or less workers.
    public enum SmallBusiness
    {
        GAMEDEVELOPMENT,
        HEALTHCLINIC,
        ITCONSULTANCY,
        MEDIA,
        LABORATORY,
        FOODCHAIN,
        RETAILER,
        AGEDCARE,
        HOSPITALITY,
        NONE
    }

    // Medium business is 50 or less workers.
    public enum MediumBusiness
    {
        GAMEDEVELOPMENT,
        HEALTHCLINIC,
        ITCONSULTANCY,
        INSURANCE,
        MEDIA,
        LABORATORY,
        PRIMARYSCHOOL,
        HIGHSCHOOL,
        FOODCHAIN,
        RETAILER,
        AGEDCARE,
        HOSPITAL,
        HOSPITALITY,
        WELFARE,
        NONE
    }

    // Large business is geater than 100 workers.
    public enum LargeBusiness
    {
        GAMEDEVELOPMENT,
        HEALTHCLINIC,
        ITCONSULTANCY,
        INSURANCE,
        MEDIA,
        LABORATORY,
        PRIMARYSCHOOL,
        HIGHSCHOOL,
        UNIVERSITY,
        COMMUNITYCOLLEGE,
        FOODCHAIN,
        RETAILER,
        AGEDCARE,
        HOSPITAL,
        HOSPITALITY,
        GOVERNMENT,
        COUNCIL,
        WELFARE,
        NONE
    }


    /// <summary>
    /// Overloaded Constructor.
    /// </summary> 
    public Business(BusinessType businessType, BusinessSize businessSize, int amountOfPlayers)
    {
        businessRoles = new List<BusinessRole>();

        this.businessType = businessType;
        this.businessSize = businessSize;

        switch (businessType)
        {
            case BusinessType.ITCONSULTANCY:
                switch (businessSize)
                {
                    case BusinessSize.SMALL:
                        // Create CEO's.
                        for (int i = 0; i < 1; i++)
                        {
                            workers.Add(new Worker(new PersonalityProfile_PassiveAggressivePrick(),
                                new BusinessRole(businessType, BusinessRole.ITConsultancy.CEO.ToString())));

                            // Create Project Managers (And Project Assistants).
                            for (int k = 0; k < 1; k++)
                            {
                                workers.Add(new Worker(new PersonalityProfile_PassiveAggressivePrick(),
                                    new BusinessRole(businessType, BusinessRole.ITConsultancy.PROJECTMANAGER.ToString())));
                                /* if (businessHasProjectAssistants) {
                                 *     workers.Add(new Worker(new PersonalityProfile(PersonalityProfile.PersonalityProfileType.PASSIVEAGGRESIVEPRICK),
                                 *     new BusinessRole(BusinessRole.ITConsultancy.PROJECTASSISTANT.ToString())));
                                 * }
                                 */

                                // Create Team Managers.
                                for (int j = 0; j < 3; j++)
                                {
                                    workers.Add(new Worker(new PersonalityProfile_PassiveAggressivePrick(),
                                        new BusinessRole(businessType, BusinessRole.ITConsultancy.TEAMMANAGER.ToString())));

                                    // Create Team Members.
                                    for (int p = 0; p < 5; p++)
                                    {
                                        // Create players until it reaches the player amount, then create A.I.'s.
                                        if (i < amountOfPlayers)
                                        {
                                            workers.Add(new Worker(new PersonalityProfile_Player(),
                                                new BusinessRole(businessType, BusinessRole.ITConsultancy.TEAMMEMBER.ToString())));
                                        }
                                        else
                                        {
                                            workers.Add(new Worker(new PersonalityProfile_PassiveAggressivePrick(),
                                                new BusinessRole(businessType, BusinessRole.ITConsultancy.TEAMMEMBER.ToString())));
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case BusinessSize.MEDIUM:
                        break;
                    case BusinessSize.LARGE:
                        break;
                    default:
                        break;
                }
                break;
            case BusinessType.FOODCHAIN:
                switch (businessSize)
                {
                    case BusinessSize.SMALL:
                        break;
                    case BusinessSize.MEDIUM:
                        break;
                    case BusinessSize.LARGE:
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
}
